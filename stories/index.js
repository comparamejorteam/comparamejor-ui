import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import withTests from 'storybook-addon-jest';
import jestTestResults from '../.jest-test-results.json';

// Call stories from everywhere
const req = require.context('../src/components', true, /\.story\.js$/)
const loadStories = () => { req.keys().forEach(filename => req(filename)) }
loadStories()