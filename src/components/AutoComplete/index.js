import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const AutoCompleteContainer = styled.ul`
  width: 100%;
  padding-left: 0;
  border-radius: 3px;
  max-height: 90px;
  overflow-y: scroll;
  top: 100%;
  position: absolute;
  z-index: 10000;
  box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0.05);
  margin-top: 0;
  ::-webkit-scrollbar {
    width: 10px;
  }

  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.2);
    border-radius: 6px;
    background-color: #fff;
  }

  ::-webkit-scrollbar-thumb {
    background: rgba(0, 0, 0, 0.2);
    border-radius: 6px;
  }

  & li {
    height: 45px;
    line-height: 45px;
    box-sizing: initial;
    padding: 0 13px;
    background-color: #f7f7f7;
    font-family: 'Roboto', sans-serif;
    list-style: none;
    text-align: center;
    color: #929292;
    font-size: 17px;
    border-radius: 3px;
    border-bottom: 2px solid #fff;
    transition: 0.3s;
    &:hover {
      background-color: #f2f2f2;
      cursor: pointer;
    }
  }

  @media screen and (min-width: 768px) {
    max-height: 130px;
  }
`;

class AutoComplete extends React.Component {
  static Option = props => (
    /* eslint-disable-next-line */
    <li onClick={() => props.changeValue({ selectValue: props.value, value: props.children })} >{props.children}</li>
  );

  render() {
    return <AutoCompleteContainer>{this.props.children}</AutoCompleteContainer>;
  }
}

AutoComplete.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
};

export default AutoComplete;
