import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';
import Select from './';
import AutoComplete from '../AutoComplete';

const data = ['Cédula de ciudadanía', 'Tarjeta de identidad'];

storiesOf('Select', module)
  .addDecorator(withTests('Select'))
  .add(
    'Basic Usage',
    withInfo(`
      ~~~js
      You will need to provide a name and a content
      <Select
        name="Holi"
      />
      ~~~
    `)(() => (
      <div>
        <Select
          name="identification"
          onChange={console.log}
          render={onClick => (
            <AutoComplete>
              {data.map(item => (
                <AutoComplete.Option value={item} key={item} changeValue={onClick}>
                  {item}
                </AutoComplete.Option>
              ))}
            </AutoComplete>
          )}
        />
      </div>
    ))
  );
