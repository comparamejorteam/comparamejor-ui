import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Icon from '../Icon';
import Alert from '../Alerts';
import AutoComplete from '../AutoComplete';

const SelectContainer = styled.div`
  width: 340px;
  max-width: 100%;
  position: relative;
`;

const SelectStyle = styled.div`
  width: 100%;
  max-width: 340px;
  height: 40px;
  background-color: #ecedef;
  border: none;
  border-radius: 3px;
  padding: 3px 28px 3px 8px;
  text-align: center;
  margin-top: 5px;
  font-size: 17px;
  color: #8f8f8f;
  box-sizing: border-box;
  outline: none;
  cursor: pointer;
  position: relative;
  font-size: 16px;
  font-family: 'Roboto', sans-serif;
  color: #606060;

  span {
    line-height: 30px;
  }

  svg {
    transition: 0.3s all;
    position: absolute;
    right: 10px;
    ${props => `transform: rotate(${props.iconRotate ? '-90deg' : '90deg'})`};
  }
`;

class Select extends React.Component {
  state = {
    isVisible: false,
    iconRotate: false,
    text: this.props.placeholder,
    selectValue: '',
  };

  componentDidMount() {
    this.myRef.name = this.props.name;
    if (this.props.required) {
      this.props.onChange({
        input: this.myRef,
        validated: false,
        value: this.state.selectValue,
      });
    } else {
      this.props.onChange({
        input: this.myRef,
        validated: true,
        value: this.state.selectValue,
      });
    }
  }

  handleItemClick = props => {
    this.setState(
      {
        text: props.value,
        isVisible: false,
        iconRotate: false,
        selectValue: props.selectValue,
      },
      () => {
        this.handleSubmit();
      }
    );
  };

  toggleList = () => {
    this.setState({
      isVisible: !this.state.isVisible,
      iconRotate: !this.state.iconRotate,
    });
  };

  hiddenList = () => {
    this.setState({
      isVisible: false,
      iconRotate: false,
    });
  };

  showList = () => {
    this.setState({
      isVisible: true,
      iconRotate: true,
    });
  };

  handleSubmit = () => {
    this.myRef.name = this.props.name;
    this.props.onChange({
      input: this.myRef,
      validated: true,
      value: this.state.selectValue,
    });
  };

  render() {
    return (
      <SelectContainer>
        <SelectStyle
          name={this.props.name}
          onClick={this.toggleList}
          iconRotate={this.state.iconRotate}
          innerRef={comp => {
            this.myRef = comp;
          }}
        >
          <span>{this.state.text}</span>
          <Icon icon="arrow" type="funnel" size="medium" onClick={this.toggleList} />
        </SelectStyle>
        {this.state.isVisible && this.props.render(this.handleItemClick)}
        {this.props.errors.error && (
          <Alert data-testid="inputError" error>
            {this.props.errors.message}
          </Alert>
        )}
      </SelectContainer>
    );
  }
}

Select.propTypes = {
  name: PropTypes.string,
  placeholder: PropTypes.string,
  errors: PropTypes.shape({
    error: PropTypes.bool,
    message: PropTypes.string,
  }),
  render: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool,
};

Select.defaultProps = {
  name: '',
  errors: {},
  placeholder: 'Selecciona una opción',
  required: false,
};

export default Select;
