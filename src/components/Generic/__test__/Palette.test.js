import React from 'react';
import styled from 'styled-components';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import { Palette } from '../';

test('Palette', () => {
  const tree = renderer.create(<Palette color="blue" />).toJSON();
  expect(tree).toMatchSnapshot();
});
