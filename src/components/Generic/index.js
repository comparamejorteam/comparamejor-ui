import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { colors } from '../../shared/variables';

const CopyInput = ({ color }) => (
  <input id={`ColorClipboard-${color}`} type="text" value={colors[color]} readOnly />
);

CopyInput.propTypes = {
  color: PropTypes.string.isRequired,
};

const Palette = styled.div.attrs({
  bgColor: ({ color }) => colors[color],
  children: props => <CopyInput {...props} />,
  onClick: ({ color }) => () => {
    const copyText = document.getElementById(`ColorClipboard-${color}`);
    copyText.select();
    document.execCommand('Copy');
  },
})`
  align-items: center;
  background: ${props => props.bgColor};
  border: 5px solid #fff;
  border-radius: 10px;
  box-sizing: border-box;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, .18);
  color: ${colors.black};
  cursor: pointer;
  font-family: 'Arial';
  font-size: 16px;
  display: flex;
  justify-content: center;
  height: 175px;
  margin: 10px;
  position: relative;
  overflow: hidden;
  width: 225px;
  transition: .3s;

  &:hover {
    transform: scale(1.05, 1.1);
  }
  &:active {
    transform: scale(1, .95);
  }
  
  &:before {
    align-items: center;
    background-color: ${colors.white};
    box-shadow: 0px 2px 0px ${colors.white};
    content: "${props => props.color}";
    display: flex;
    height: 50px;
    padding-left: 15px;
    text-transform: uppercase;
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
  }

  input {
    background-color: transparent;
    border: none;
    cursor: pointer;
    font-size: 14px;
    font-weight: bold;
    position: absolute;
    left: 15px;
    right: 15px;
    bottom: 17.5px;
    text-align: right;
    width: 185px;

    &:hover, &:focus {
      outline: none;
    }
  }
`;

const PaletteContainer = styled.div`
  display: flex;
  flex-wrap: wrap;

  @media screen and (max-width: 767px) {
    padding: 50px 0;
    justify-content: center;
  }
`;

PaletteContainer.propTypes = {
  children: ((...types) => {
    const fieldType = PropTypes.shape({
      type: PropTypes.oneOf(types),
    });
    return PropTypes.oneOfType([fieldType, PropTypes.arrayOf(fieldType)]);
  })(Palette),
};

export { Palette, PaletteContainer };
