import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';
import { Palette, PaletteContainer } from './';

storiesOf('Palette', module)
  .addDecorator(withTests('Palette'))
  .add(
    'Colors',
    withInfo(`
      ~~~js
      // Basic usage
      <Button
        type="primary"
        size="large"
        text="Cotiza Gratis"
      />
      
      // Button customization
      <Button
        type="primary | secondary | funnel | funnel-secondary"
        size="large | medium | small"
        text="Cotiza Gratis"
        withIcon="arrow | loading"
      />
      ~~~
    `)(() => (
      <PaletteContainer>
        <Palette color="black" />
        <Palette color="gray" />
        <Palette color="darkGray" />

        <Palette color="blue" />
        <Palette color="skyBlue" />
        <Palette color="darkBlue" />
        <Palette color="lightBlue" />
        <Palette color="hoverBlue" />

        <Palette color="yellow" />
        <Palette color="darkYellow" />

        <Palette color="green" />
        <Palette color="darkGreen" />
      </PaletteContainer>
    ))
  );
