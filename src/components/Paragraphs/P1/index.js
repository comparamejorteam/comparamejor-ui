import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { colors } from '../../../shared/variables';

const P1 = styled.p`
  font-family: Roboto, sans-serif;
  color: inherit;
  font-size: ${props => props.fontSize || '20px'};
  @media screen and (max-width: 767px) {
    font-size: 18px;
  }
  ${props =>
    props.isDark &&
    css`
      color: ${colors.darkGray};
    `};
  ${props =>
    props.isLight &&
    css`
      color: ${colors.gray};
    `};
`;

P1.propTypes = {
  isDark: PropTypes.bool,
  isLight: PropTypes.bool,
};

P1.defaultProps = {
  isDark: false,
  isLight: false,
};

export default P1;
