import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { colors } from '../../../shared/variables';

const P3 = styled.p`
  font-family: Roboto, sans-serif;
  font-size: ${props => props.fontSize || '14px'};
  color: inherit;
  @media screen and (max-width: 767px) {
    font-size: 12px;
  }
  ${props =>
    props.isDark &&
    css`
      color: ${colors.darkGray};
    `};
  ${props =>
    props.isLight &&
    css`
      color: ${colors.gray};
    `};
`;

P3.propTypes = {
  isDark: PropTypes.bool,
  isLight: PropTypes.bool,
};

P3.defaultProps = {
  isDark: false,
  isLight: false,
};

export default P3;
