import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';
import P from './';

const { P1, P2, P3 } = P;

storiesOf('Paragraphs')
  .addDecorator(withTests('P1', 'P2', 'P3'))
  .add(
    'Paragraph Level 1',
    withInfo(`

      ~~~js
      // Basic usage
      <P1 fontSize="20px">Paragraph 1</P1>
      <P1 isDark fontSize="20px">Paragraph 1</P1>
      ~~~

    `)(() => (
      <div>
        <P1 isLight fontSize="20px">
          Paragraph 1 Light
        </P1>
        <P1 isDark fontSize="20px">
          Paragraph 1 Dark
        </P1>
      </div>
    ))
  )
  .add(
    'Paragraph Level 2',
    withInfo(`

      ~~~js
      // Basic usage
      <P2 isLight fontSize="18px">Paragraph 2</P2>
      <P2 isDark fontSize="18px">Paragraph 2</P2>
      ~~~

    `)(() => (
      <div>
        <P2 isLight fontSize="18px">
          Paragraph 2 Light
        </P2>
        <P2 isDark fontSize="18px">
          Paragraph 2 Dark
        </P2>
      </div>
    ))
  )
  .add(
    'Paragraph Level 3',
    withInfo(`

      ~~~js
      // Basic usage
      <P3 isLight fontSize="14px">Paragraph 3</P3>
      <P3 isDark fontSize="14px">Paragraph 3</P3>
      ~~~

    `)(() => (
      <div>
        <P3 isLight fontSize="14px">
          Paragraph 3 Light
        </P3>
        <P3 isDark fontSize="14px">
          Paragraph 3 Dark
        </P3>
      </div>
    ))
  );
