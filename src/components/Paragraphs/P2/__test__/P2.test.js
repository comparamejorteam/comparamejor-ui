import React from 'react';
import styled from 'styled-components';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import P2 from '../';

test('The size is correct. ', () => {
  const tree = renderer.create(<P2 />).toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toHaveStyleRule('font-size', '18px');
});

test('The color is correct. ', () => {
  const tree = renderer.create(<P2 />).toJSON();
  expect(tree).toMatchSnapshot();
  // expect(tree).toHaveStyleRule('color', '#004b8c');
});

test('The font family is correct. ', () => {
  const tree = renderer.create(<P2 />).toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toHaveStyleRule('font-family', 'Roboto,sans-serif');
});
