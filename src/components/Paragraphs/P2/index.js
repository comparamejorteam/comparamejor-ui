import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { colors } from '../../../shared/variables';

const P2 = styled.p`
  font-family: Roboto, sans-serif;
  font-size: ${props => props.fontSize || '18px'};
  color: inherit;
  @media screen and (max-width: 767px) {
    font-size: 16px;
  }
  ${props =>
    props.isDark &&
    css`
      color: ${colors.darkGray};
    `};
  ${props =>
    props.isLight &&
    css`
      color: ${colors.gray};
    `};
`;

P2.propTypes = {
  isDark: PropTypes.bool,
  isLight: PropTypes.bool,
};

P2.defaultProps = {
  isDark: false,
  isLight: false,
};

export default P2;
