import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Alert from '../Alerts';

const CheckMark = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  height: 16px;
  width: 16px;
  border-radius: 3px;
  display: block;
  border: 2px solid #939393;

  &::after {
    content: '';
    position: absolute;
    display: block;
    left: 1px;
    top: 1px;
    width: 10px;
    height: 10px;
    border-radius: 3px;
    background-color: #00b800;
    transform: scale(0);
    transition: 0.3s all;
  }
`;

const Input = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;

  &:checked ~ ${CheckMark} {
    border-color: #00b800;
  }

  &:checked ~ ${CheckMark}::after {
    transform: scale(1);
  }
`;

const Container = styled.label`
  display: block;
  position: relative;
  padding-left: 30px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 14px;
  color: #929292;
  font-family: 'Roboto', sans-serif;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  &:hover ${Input} ~ ${CheckMark} {
    border-color: #00b800;
  }
`;

class Checkbox extends React.Component {
  state = {
    checked: this.props.checked,
  };

  componentDidMount() {
    if (!this.props.required) {
      this.isValidated(true);
    } else {
      this.isValidated(false);
    }
  }

  isValidated = validated => {
    this.props.onChange({
      input: this.myRef,
      validated,
      value: this.myRef.value,
    });
  };

  toggleChecked = () => {
    this.setState({
      checked: !this.state.checked,
    });
  };

  render() {
    return (
      <Container data-testid="radio">
        {this.props.text}
        <Input
          type="checkbox"
          name={this.props.name}
          value={this.props.value}
          required={this.props.required}
          onChange={() => this.isValidated(true)}
          innerRef={comp => {
            this.myRef = comp;
          }}
          checked={this.state.checked}
          onClick={this.toggleChecked}
        />
        <CheckMark />
        {this.props.errors.error && (
          <div>
            <Alert error>{this.props.errors.message}</Alert>
          </div>
        )}
      </Container>
    );
  }
}

Checkbox.propTypes = {
  name: PropTypes.string,
  text: PropTypes.string,
  value: PropTypes.string,
  required: PropTypes.bool,
  onChange: PropTypes.func,
  errors: PropTypes.shape({
    error: PropTypes.bool,
    message: PropTypes.string,
  }),
  checked: PropTypes.bool,
};

Checkbox.defaultProps = {
  name: '',
  text: '',
  value: '',
  required: false,
  errors: {},
  onChange: () => {},
  checked: false,
};

export default Checkbox;
