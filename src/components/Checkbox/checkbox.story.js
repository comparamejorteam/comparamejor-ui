import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';

import Checkbox from './';

storiesOf('Checkbox', module).add(
  'Checkbox',
  withInfo(`
    ~~~js
    // Basic usage
    <Checkbox
      name="gender"
      text="Hombre"
      value="male"
      checked={true | false}
    />
    ~~~
  `)(() => (
    <div style={{ padding: '1rem' }}>
      <Checkbox
        name="news"
        value="news"
        checked
        text="Boletín de noticias semanal con información sobre seguros y finanzas personales."
      />
    </div>
  ))
);
