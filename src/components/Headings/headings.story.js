import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';
import H from './';

const { H1, H2, H3, H4, H5 } = H;

storiesOf('Titles')
  .addDecorator(withTests('Heading1', 'Heading2', 'Heading3', 'Heading4', 'Heading5'))
  .add(
    'Heading Level 1',
    withInfo(`

      ~~~js
      // Basic usage
      <Heading1 fontSize="45px">Heading 1</Heading1>
      ~~~

    `)(() => <H1 fontSize="45px">H1</H1>)
  )
  .add(
    'Heading Level 2',
    withInfo(`

      ~~~js
      // Basic usage
      <H2 fontSize="36px">H 2</H2>
      ~~~

    `)(() => <H2 fontSize="36px">H2</H2>)
  )
  .add(
    'Heading Level 3',
    withInfo(`

      ~~~js
      // Basic usage
      <H3 fontSize="32px">H 3</H3>
      ~~~

    `)(() => <H3 fontSize="32px">H3</H3>)
  )
  .add(
    'Heading Level 4',
    withInfo(`

      ~~~js
      // Basic usage
      <H4 fontSize="28px">H 4</H4>
      ~~~

    `)(() => <H4 fontSize="28px">H4</H4>)
  )
  .add(
    'Heading Level 5',
    withInfo(`

      ~~~js
      // Basic usage
      <H5 fontSize="24px">H 5</H5>
      ~~~

    `)(() => <H5 fontSize="24px">H5</H5>)
  );
