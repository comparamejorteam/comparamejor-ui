import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const H1 = styled.h1`
  color: #004b8c;
  font-size: 45px;
  font-family: 'DecourSoft', sans-serif;
  text-align: ${props => props.align};

  @media screen and (max-width: 767px) {
    font-size: 35px;
  }
`;

H1.propTypes = {
  align: PropTypes.string,
};

H1.defaultProps = {
  align: 'left',
};

export default H1;
