import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const H5 = styled.h5`
  color: #004b8c;
  font-size: 24px;
  font-family: 'DecourSoft', sans-serif;
  text-align: ${props => props.align};

  @media screen and (max-width: 767px) {
    font-size: 18px;
  }
`;

H5.propTypes = {
  align: PropTypes.string,
};

H5.defaultProps = {
  align: 'left',
};

export default H5;
