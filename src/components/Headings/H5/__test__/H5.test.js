import React from 'react';
import styled from 'styled-components';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import H5 from '../';

test('The size is correct.', () => {
  const tree = renderer.create(<H5 />).toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toHaveStyleRule('font-size', '24px');
});

test('The color is correct. ', () => {
  const tree = renderer.create(<H5 />).toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toHaveStyleRule('color', '#004b8c');
});
