import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const H4 = styled.h4`
  color: #004b8c;
  font-size: 28px;
  font-family: 'DecourSoft', sans-serif;
  text-align: ${props => props.align};

  @media screen and (max-width: 767px) {
    font-size: 22px;
  }
`;

H4.propTypes = {
  align: PropTypes.string,
};

H4.defaultProps = {
  align: 'left',
};

export default H4;
