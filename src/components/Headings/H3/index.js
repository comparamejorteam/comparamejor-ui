import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const H3 = styled.h3`
  color: #004b8c;
  font-size: 32px;
  font-family: 'DecourSoft', sans-serif;
  text-align: ${props => props.align};

  @media screen and (max-width: 767px) {
    font-size: 26px;
  }
`;

H3.propTypes = {
  align: PropTypes.string,
};

H3.defaultProps = {
  align: 'left',
};

export default H3;
