import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const H2 = styled.h2`
  color: #004b8c;
  font-size: 36px;
  font-family: 'DecourSoft', sans-serif;
  text-align: ${props => props.align};

  @media screen and (max-width: 767px) {
    font-size: 30px;
  }
`;

H2.propTypes = {
  align: PropTypes.string,
};

H2.defaultProps = {
  align: 'left',
};

export default H2;
