import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';

import RadioButton from './';

storiesOf('Radio Button', module)
  .addDecorator(withTests('Radio Button'))
  .add(
    'Radio',
    withInfo(`
        ~~~js
        // Basic usage
        <RadioButton
          name="gender"
          text="Hombre"
          value="male"
        />
        ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <RadioButton
          name="gender"
          value="hombre"
          text="Información relacionada con el seguro que estoy cotizando."
        />
        <RadioButton
          name="gender"
          value="mujer"
          text="Boletín de noticias semanal con información sobre seguros y finanzas personales."
        />
      </div>
    ))
  )
  .add(
    'Radio with children props',
    withInfo(`
        ~~~js
        // Basic width children
        <RadioButton
          name="gender"
          value="male"
        />
        ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <RadioButton name="information" value="quoting">
          Información relacionada con el <strong>seguro que estoy</strong> cotizando.
        </RadioButton>
        <RadioButton name="information" value="news">
          Boletín de noticias semanal con información sobre seguros y finanzas personales.
        </RadioButton>
      </div>
    ))
  );
