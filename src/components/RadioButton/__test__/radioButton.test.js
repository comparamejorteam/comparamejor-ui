import React from 'react';
import { render } from 'react-testing-library';
import RadioButton from '../';

test('Radio Button', () => {
  const { getByTestId } = render(<RadioButton name="gender" value="Male" />);

  expect(getByTestId('radio')).toBeTruthy();
});
