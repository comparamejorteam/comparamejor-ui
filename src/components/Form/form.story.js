import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';

import Form from './';

import AutoComplete from '../AutoComplete';
import cities from '../Input/cities';

storiesOf('Forms', module)
  .addDecorator(withTests('Form'))
  .add(
    'default',
    withInfo(`
      ~~~js
        // Basic usage
      <Form onSubmit={onSubmit} onChange={onChange}>
        <Form.Input type="text" placeholder="Primer nombre" name="firstName" required />
        <Form.Input type="number" placeholder="Cedula" name="cc" required />
        <Form.Submit type="funnel" size="large" text="Enviar" icon="arrow" />
      </Form>

      // Input customization
      <Form onSubmit={onSubmit}>
        <Form.Input type="text" placeholder="Primer nombre" name="firstName" required />
        <Form.Input type="number" placeholder="Cedula" name="cc" required />
        <Form.Submit type="funnel" size="large" text="Enviar" icon="arrow" />
      </Form>
      ~~~
    `)(() => (
      <div style={{ padding: '1rem', maxWidth: '468px', margin: '0 auto' }}>
        <Form onSubmit={console.log}>
          <Form.Input type="text" placeholder="Primer nombre" name="firstName" />
          <Form.Input
            type="phone"
            placeholder="celular"
            name="phone"
            max={10}
            errorMessage="numero invalido"
          />
          <Form.Input
            type="text"
            placeholder="Ciudad"
            name="city"
            required
            itemFilter="name"
            errorMessage="ciudad invalida"
            data={[
              { name: 'Neiva', id: 1 },
              { name: 'Buga', id: 2 },
              { name: 'Cabrera', id: 3 },
              { name: 'La Argentina', id: 4 },
            ]}
            autoComplete={(data, onClick) => (
              <AutoComplete>
                {data.map(city => (
                  <AutoComplete.Option changeValue={onClick} value={city.name} key={city.id}>
                    {city.name}
                  </AutoComplete.Option>
                ))}
              </AutoComplete>
            )}
          />
          <Form.InputButton
            type="plate"
            errorMessage="La placa ingresada no es válida."
            name="plate"
            placeholder="Ingresa tu placa"
            required
          />
          <div style={{ textAlign: 'left', width: '100%' }}>
            <h1>Genero</h1>
            <Form.Radio text="Hombre" checked value="hombre" name="gender" required />
            <Form.Radio text="Mujer" value="mujer" name="gender" required />
          </div>
          <Form.Submit type="primary" size="medium" text="Enviar" icon="arrow" />
        </Form>
      </div>
    ))
  )
  .add(
    'InputButton',
    withInfo(`
      ~~~js
        // Basic usage
      <Form onSubmit={onSubmit} onChange={onChange}>
        <Form.Input type="text" placeholder="Primer nombre" name="firstName" required />
        <Form.Input type="number" placeholder="Cedula" name="cc" required />
        <Form.Submit type="funnel" size="large" text="Enviar" icon="arrow" />
      </Form>

      // Input customization
      <Form onSubmit={onSubmit}>
        <Form.Input type="text" placeholder="Primer nombre" name="firstName" required />
        <Form.Input type="number" placeholder="Cedula" name="cc" required />
        <Form.Submit type="funnel" size="large" text="Enviar" icon="arrow" />
      </Form>
      ~~~
    `)(() => (
      <div style={{ padding: '1rem', maxWidth: '468px', margin: '0 auto', textAlign: 'center' }}>
        <Form onSubmit={console.log}>
          <Form.InputButton
            type="plate"
            errorMessage="La placa ingresada no es válida."
            name="plate"
            placeholder="Ingresa tu placa"
          />
        </Form>
      </div>
    ))
  );
