import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Input from '../Input';
import Button from '../Buttons';
import InputButton from '../InputButton';
import RadioButton from '../RadioButton';
import Checkbox from '../Checkbox';
import Select from '../Select';

class Form extends React.Component {
  static Input = props => <Input {...props} />;
  static Radio = props => <RadioButton {...props} />;
  static Checkbox = props => <Checkbox {...props} />;
  static Select = props => <Select {...props} />;
  static Submit = props => (
    <Button
      style={{ marginTop: '1rem' }}
      {...props}
      onClick={e => {
        e.preventDefault();
        props.onClick();
      }}
    />
  );
  static InputButton = props => <InputButton {...props} />;

  state = {
    inputs: null,
    errors: {},
  };

  handleChange = ({ input, validated, value, onChange }) => {
    const target = input && input.target ? input.target : input;

    this.setState(prevState => ({
      inputs: {
        ...prevState.inputs,
        [target.name]: {
          input: target,
          validated,
          value,
        },
      },
    }));

    if (this.props.onChange) {
      this.props.onChange({ input, validated });
    }
  };

  isErrors = () => {
    let errors = {};
    Object.entries(this.state.inputs).forEach(([key, value]) => {
      errors = {
        ...errors,
        [key]: {
          error: !value.validated,
          message:
            !this.state.inputs[key].input.value && 'Es necesario que contestes esta pregunta',
        },
      };
    });
    this.setState(
      () => ({
        errors,
      }),
      () => this.handleSubmit(errors)
    );
  };

  handleSubmit = validations => {
    const error = Object.getOwnPropertyNames(validations)
      .map(inputName => validations[inputName].error)
      .includes(true);

    if (!error) {
      this.props.onSubmit(this.state.inputs);
    }
  };

  render() {
    const { errors } = this.state;

    return (
      <form data-testid="form" className={this.props.className}>
        {React.Children.map(this.props.children, child => {
          if (child.props.type === 'plate') {
            return React.cloneElement(child, {
              errors: {
                error: errors[child.props.name] && errors[child.props.name].error,
                message:
                  errors[child.props.name] &&
                  (errors[child.props.name].message || child.props.errorMessage),
              },
              onClick: this.isErrors,
              onChange: this.handleChange,
            });
          }

          if (child.props.text) {
            return React.cloneElement(child, {
              onClick: this.isErrors,
            });
          }

          if (child.type === 'div') {
            return React.cloneElement(
              child,
              child.props,
              React.Children.map(child.props.children, children =>
                React.cloneElement(children, {
                  errors: {
                    error: errors[child.props.name] && errors[child.props.name].error,
                    message:
                      errors[children.props.name] &&
                      (errors[children.props.name].message || children.props.errorMessage),
                  },
                  onChange: this.handleChange,
                })
              )
            );
          }

          return React.cloneElement(child, {
            errors: {
              error: errors[child.props.name] && errors[child.props.name].error,
              message:
                errors[child.props.name] &&
                (errors[child.props.name].message || child.props.errorMessage),
            },
            onChange: this.handleChange,
          });
        })}
      </form>
    );
  }
}

Form.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.object])
    .isRequired,
  onSubmit: PropTypes.func,
  onChange: PropTypes.func,
  errorMessage: PropTypes.string,
};

Form.defaultProps = {
  className: '',
  onSubmit: () => {},
  onChange: null,
  errorMessage: 'ingrese un valor valido',
};

export default Form;
