import React from 'react';
import { render, Simulate, wait } from 'react-testing-library';
import Form from '../';
import AutoComplete from '../../AutoComplete';

import cities from '../../Input/cities';

const onSubmit = jest.fn();

beforeEach(() => {
  jest.resetAllMocks();
});

describe('Form', async () => {
  test('render', () => {
    const { getByTestId, getByLabelText, getByText } = render(
      <Form onSubmit={onSubmit}>
        <Form.Input
          type="text"
          label="Primer nombre"
          placeholder="Primer nombre"
          name="firstName"
          required
        />
        <Form.Input type="number" label="Cedula" placeholder="Cedula" name="cc" required />
        <Form.Submit type="funnel" size="small" text="Enviar" icon="arrow" />
      </Form>
    );

    getByLabelText('Primer nombre').value = 'comparamejor';
    Simulate.change(getByLabelText('Primer nombre'));

    getByLabelText('Cedula').value = '1234567890';
    Simulate.change(getByLabelText('Cedula'));

    Simulate.click(getByText('Enviar'));

    expect(onSubmit).toBeCalled();
    expect(onSubmit.mock.calls.length).toBe(1);
  });

  test('errorMessage with props', () => {
    const { getByTestId, getByLabelText, getByText } = render(
      <Form onSubmit={onSubmit}>
        <Form.Input
          type="plate"
          errorMessage="ingrese una placa valida"
          label="Placa"
          placeholder="Placa"
          name="plate"
          required
        />
        <Form.Submit type="funnel" size="small" text="Enviar" icon="arrow" />
      </Form>
    );

    getByLabelText('Placa').value = 'jhg54121';
    Simulate.change(getByLabelText('Placa'));

    Simulate.click(getByText('Enviar'));

    expect(getByTestId('inputError').textContent).toBe('ingrese una placa valida');
  });

  test('errorMessage without props', () => {
    const { getByTestId, getByLabelText, getByText } = render(
      <Form onSubmit={onSubmit}>
        <Form.Input type="plate" label="Placa" placeholder="Placa" name="plate" required />
        <Form.Submit type="funnel" size="small" text="Enviar" icon="arrow" />
      </Form>
    );

    getByLabelText('Placa').value = '';
    Simulate.change(getByLabelText('Placa'));

    Simulate.click(getByText('Enviar'));

    expect(getByTestId('inputError').textContent).toBe('Es necesario que contestes esta pregunta');
  });

  test('submit with errors', () => {
    const { getByTestId, getByPlaceholderText, getByText } = render(
      <Form onSubmit={onSubmit}>
        <Form.Input
          type="text"
          placeholder="seleccione una ciudad"
          name="city"
          select
          required
          data={[
            { name: 'Neiva', id: 1 },
            { name: 'Buga', id: 2 },
            { name: 'Cabrera', id: 3 },
            { name: 'La Argentina', id: 4 },
          ]}
          autoComplete={(data, onClick) => (
            <AutoComplete>
              {data.map(city => (
                <AutoComplete.Option changeValue={onClick} value={city.name} key={city.id}>
                  {city.name}
                </AutoComplete.Option>
              ))}
            </AutoComplete>
          )}
        />
        <Form.Submit type="funnel" size="small" text="Enviar" icon="arrow" />
      </Form>
    );

    Simulate.click(getByText('Enviar'));

    expect(onSubmit.mock.calls.length).toBe(0);
  });
});
