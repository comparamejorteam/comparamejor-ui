import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { colors } from '../../shared/variables';

class Modal extends React.Component {
  static propTypes = {
    controls: PropTypes.func.isRequired,
    isOpen: PropTypes.bool,
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    isOpen: PropTypes.true,
  };

  state = {
    isOpen: this.props.isOpen || true,
  };

  render() {
    const { isOpen } = this.state;
    const { controls: Controls, children, ...props } = this.props;

    return (
      <div>
        {/* {controls({ openModal: () => this.setState({ isOpen: true }) })} */}
        <Controls openModal={() => this.setState({ isOpen: true })} />

        {isOpen && (
          <Exclude>
            <GrayScale onClick={() => this.setState({ isOpen: false })} />
            <ModalContainer>{children}</ModalContainer>
          </Exclude>
        )}
      </div>
    );
  }
}

const Exclude = styled.div`
  align-items: center;
  display: inline-flex;
  justify-content: center;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  z-index: 1;
`;

const GrayScale = styled.div`
  background-color: rgba(0, 0, 0, 0.4);
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -1;
`;

const ModalContainer = styled.div`
  align-items: center;
  background-color: ${colors.yellow};
  border: 1px solid ${colors.gray};
  border-radius: 10px;
  color: ${colors.darkBlue};
  display: flex;
  font-size: 22px;
  flex-wrap: wrap;
  justify-content: center;
  line-height: 1.4;
  max-width: 500px;
  min-height: 175px;
  min-width: 300px;
  padding: 25px 50px;
  position: relative;
  text-align: center;
  z-index: 1;
`;

export default Modal;
