import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';
import Modal from './';
import Button from '../Buttons';
import H from '../Headings';
import P from '../Paragraphs';

const { H1, H2, H3, H4, H5 } = H;
const { P1, P2, P3 } = P;

storiesOf('Modals')
  .addDecorator(withTests('Modal'))
  .add(
    'Simple Modal',
    withInfo(`
    The 'controls' will render your components normaly, but you will get super powers for
    control the modal (which you can control in the render prop).

    ~~~js
    <Modal
      controls={({ openModal }) => (
        // Your components to control the modal
      )}
    />
    ~~~

  `)(() => (
      <div>
        <Modal
          controls={({ openModal }) => (
            <Button
              type="secondary"
              size="medium"
              text="Open Modal"
              onClick={openModal}
              icon="none"
            />
          )}
        >
          <H4>¿Estás seguro que quieres regresar a la pregunta anterior?</H4>
          <P2>¿Estás seguro que quieres regresar a la pregunta anterior?</P2>
          <Button size="medium" text="Seguir cotizando" icon="arrow" />
        </Modal>
      </div>
    ))
  );
