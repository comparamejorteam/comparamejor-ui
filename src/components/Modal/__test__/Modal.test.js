import React from 'react';
import styled from 'styled-components';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import Modal from '../';

test('it works', () => {
  const tree = renderer.create(<Modal controls={() => <p>Holi</p>}>contenidoyarn </Modal>).toJSON();
  expect(tree).toMatchSnapshot();
  // expect(tree).toHaveStyleRule('color', '#42b715');
});
