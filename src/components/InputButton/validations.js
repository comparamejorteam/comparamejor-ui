export const plate = string => {
  /* eslint-disable-next-line */
  const re = /^[a-zA-Z]{2,3}[0-9]{2}[a-zA-Z0-9]?$/;

  if (re.test(String(string).toLowerCase())) return true;
  return false;
};
