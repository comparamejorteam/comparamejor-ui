import React from 'react';
import { render } from 'react-testing-library';
import InputButton from '../';

test('Input Button', () => {
  const { getByTestId } = render(<InputButton name="plate" />);

  expect(getByTestId('InputButton')).toBeTruthy();
});
