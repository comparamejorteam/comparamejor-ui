import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';

import InputButton from './';

storiesOf('Input Button', module)
  .addDecorator(withTests('Input Button'))
  .add(
    'Input Button',
    withInfo(`
        ~~~js
        // Basic usage
        <InputButton
          name="gender"
          text="Hombre"
          value="male"
        />
        ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <InputButton name="vehicleRegistration" placeholder="Ingresa tu placa" />
      </div>
    ))
  );
