import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Buttons';
import Alert from '../Alerts';
import * as validations from '../Input/validations';

const InputButtonContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const Input = styled.input`
  height: auto;
  width: 234px;
  border: none;
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
  padding: 3px 10px;
  background: #fff;
  border: none;
  text-align: center;
  font-size: 17px;
  color: #8f8f8f;
  box-sizing: border-box;
  outline: none;
  @media screen and (max-width: 576px) {
    width: 100%;
    border-top-left-radius: 6px;
    border-top-right-radius: 6px;
    border-bottom-left-radius: 0;
    height: 56px;
  }
`;

const ButtonInput = styled(Button)`
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
  @media screen and (max-width: 576px) {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-bottom-left-radius: 6px;
    border-bottom-right-radius: 6px;
  }
`;

class InputButton extends React.Component {
  state = {
    input: null,
    value: '',
  };

  componentDidMount() {
    if (!this.props.required) {
      this.isValidated(true);
    } else {
      this.isValidated(false);
    }
  }

  typing = e => {
    e.persist();
    const { value } = e.target;

    if (value.length - 1 >= 6) {
      return;
    }

    if (this.props.type === 'plate') {
      this.changeInputValue(e, () => this.isValidated(false));
      if (validations.plate(value)) {
        this.changeInputValue(e, () => this.isValidated(true));
      }
    }
  };

  changeInputValue = (input, callback) => {
    this.setState({ input, value: input.target ? input.target.value : '' }, callback);
  };

  isValidated = validated => {
    this.props.onChange({
      input: this.state.input || this.myRef,
      validated,
    });
  };

  render() {
    return (
      <InputButtonContainer>
        <Input
          data-testid="InputButton"
          type="plate"
          name={this.props.name}
          placeholder={this.props.placeholder}
          required={this.props.required}
          innerRef={comp => {
            this.myRef = comp;
          }}
          onChange={this.typing}
          max={6}
          value={this.state.value.toUpperCase()}
        />
        <ButtonInput
          onClick={e => {
            e.preventDefault();
            this.props.onClick();
          }}
          type="primary"
          size="large"
          text="Cotiza Gratis"
        />
        {this.props.errors.error && (
          <div>
            <Alert error>{this.props.errors.message}</Alert>
          </div>
        )}
      </InputButtonContainer>
    );
  }
}

InputButton.propTypes = {
  name: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  required: PropTypes.bool,
  errors: PropTypes.shape({
    error: PropTypes.bool,
    message: PropTypes.string,
  }),
  onChange: PropTypes.func,
  onClick: PropTypes.func,
};

InputButton.defaultProps = {
  name: '',
  placeholder: '',
  type: 'text',
  required: false,
  errors: {},
  onChange: () => {},
  onClick: () => {},
};

export default InputButton;
