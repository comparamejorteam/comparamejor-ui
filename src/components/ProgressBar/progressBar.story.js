import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';

import ProgressBar from './';

storiesOf('ProgressBar', module).add(
  'ProgressBar',
  withInfo(`
    ~~~js
    // Basic usage
    <ProgressBar
      progress={0-100}
    />
    ~~~
  `)(() => (
    <div style={{ padding: '1rem', width: '768px', backgroundColor: '#009BD2' }}>
      <ProgressBar progress={50} />
    </div>
  ))
);
