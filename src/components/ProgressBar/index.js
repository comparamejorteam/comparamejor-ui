import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { colors } from '../../shared/variables';

const Bar = styled.div`
  width: ${props => `${Number(props.progress)}%`};
  max-width: 1500px;
  height: 6px;
  background-color: ${colors.yellow};
  border-radius: 4px;
  position: relative;
  transition: 0.3s all;

  &::before {
    content: '';
    width: 20px;
    height: 20px;
    background-color: ${colors.yellow};
    border-radius: 50%;
    position: absolute;
    top: -7px;
    right: -5px;
  }
`;

const Container = styled.div`
  width: 100%;
  padding: 4px;
  background-color: rgba(255, 255, 255, 0.2);
  border-radius: 8px;
  max-width: 1500px;
  margin: 0 auto;
`;

const ProgressBar = props => (
  <Container>
    <Bar progress={props.progress} />
  </Container>
);

ProgressBar.propTypes = {
  progress: PropTypes.number,
};

ProgressBar.defaultProps = {
  progress: 0,
};

export default ProgressBar;
