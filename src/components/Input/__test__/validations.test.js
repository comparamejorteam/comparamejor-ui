import * as validations from '../validations';

describe('validations', () => {
  test('number', () => {
    expect(validations.number('abc')).toBe(false);
    expect(validations.number('123abc')).toBe(false);
    expect(validations.number('123')).toBe(true);
  });

  test('phone', () => {
    expect(validations.phone('3177458944')).toBe(true);
    expect(validations.phone('aasdassds4')).toBe(false);
    expect(validations.phone('31774589443')).toBe(false);
  });

  test('email', () => {
    expect(validations.email('cristian.vargas@comparamejor.com')).toBe(true);
    expect(validations.email('cristian.vargas@comparamejor')).toBe(false);
    expect(validations.email('cristian@compara.c')).toBe(false);
    expect(validations.email('cristian@asd.com.co')).toBe(true);
    expect(validations.email('cristian@.com.co')).toBe(false);
    expect(validations.email('cristian.com.co')).toBe(false);
    expect(validations.email('@.com.co')).toBe(false);
  });

  test('plate', () => {
    expect(validations.plate('1234asd')).toBe(false);
    expect(validations.plate('ADS345')).toBe(true);
    expect(validations.plate('1a1s2d')).toBe(false);
  });
});
