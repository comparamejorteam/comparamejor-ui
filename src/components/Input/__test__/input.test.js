import React from 'react';
import { render, Simulate, wait } from 'react-testing-library';
import cities from '../cities';
import Input from '../';
import AutoComplete from '../../AutoComplete';

const onChange = jest.fn();

beforeEach(() => {
  jest.resetAllMocks();
});

describe('Input', () => {
  test('render default input', () => {
    const { getByTestId, getByLabelText } = render(
      <Input placeholder="Primer nombre" label="Primer nombre" name="firstName" required />
    );

    const input = getByTestId('input');

    expect(input).toBeTruthy();
    expect(input.value).toBe('');
    expect(input.required).toBe(true);
    expect(input.name).toBe('firstName');
    expect(getByLabelText('Primer nombre')).toBeTruthy();
  });

  test('change input value', () => {
    const { getByTestId, getByLabelText } = render(
      <Input
        placeholder="Primer nombre"
        label="Primer nombre"
        name="firstName"
        onChange={onChange}
        required
      />
    );

    const input = getByTestId('input');
    getByLabelText('Primer nombre').value = 'COMPARAMEJOR';
    Simulate.change(input);

    getByLabelText('Primer nombre').value = 'Comparamejor';
    Simulate.change(input);

    expect(input.value).toBe('Comparamejor');
    expect(onChange.mock.calls.length).toBe(3);
  });

  test('input type number', async () => {
    const { getByTestId, getByLabelText } = render(
      <Input placeholder="Cedula" label="Cedula" name="cc" type="number" />
    );

    const input = getByTestId('input');
    getByLabelText('Cedula').value = '123';
    Simulate.change(input);

    getByLabelText('Cedula').value = 'abc';
    Simulate.change(input);

    expect(input.value).toBe('123');
    expect(getByTestId('input').type).toBe('text');
  });

  test('input type text', () => {
    const { getByTestId, getByLabelText } = render(
      <Input name="firstName" type="text" label="Nombre" placeholder="Nombre" />
    );

    const input = getByTestId('input');

    getByLabelText('Nombre').value = 'comparamejor-123';
    Simulate.change(input);

    expect(input.value).toBe('comparamejor-123');
    expect(input.type).toBe('text');
  });

  test('input type phone', () => {
    const { getByTestId, getByLabelText } = render(
      <Input name="phone" type="phone" placeholder="Celular" label="Celular" required />
    );

    const input = getByTestId('input');
    getByLabelText('Celular').value = '3177654378';
    Simulate.change(input);

    expect(input.value).toBe('3177654378');
  });

  test('input type email', () => {
    const { getByTestId, getByLabelText } = render(
      <Input name="email" type="email" placeholder="Email" label="Email" required />
    );

    const input = getByTestId('input');
    getByLabelText('Email').value = 'cristian.vargas@comparamejor.com';
    Simulate.change(input);

    expect(input.value).toBe('cristian.vargas@comparamejor.com');
  });

  test('input max length', () => {
    const { getByTestId, getByLabelText } = render(
      <Input name="year" type="number" placeholder="Año" label="Año" max={4} required />
    );

    const input = getByTestId('input');
    getByLabelText('Año').value = '1';
    Simulate.change(input);
    getByLabelText('Año').value = '12';
    Simulate.change(input);
    getByLabelText('Año').value = '123';
    Simulate.change(input);
    getByLabelText('Año').value = '1234';
    Simulate.change(input);
    getByLabelText('Año').value = '12345';
    Simulate.change(input);

    expect(input.value).toBe('1234');
  });

  test('input min length', () => {
    const { getByTestId, getByLabelText } = render(
      <Input
        name="year"
        type="number"
        placeholder="Año"
        label="Año"
        min={3}
        required
        onChange={onChange}
      />
    );

    const input = getByTestId('input');
    getByLabelText('Año').value = '12';
    Simulate.change(input);

    expect(input.value).toBe('12');
  });
});
