import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import * as validations from './validations';
import AutoComplete from '../AutoComplete';

import Icon from '../Icon';
import Alert from '../Alerts';

const InputStyled = styled.input`
  ${props => props.type === 'plate' && 'text-transform: uppercase'};
  ${props => props.select && 'cursor: pointer'};
  width: 100%;
  height: 50px;
  background-color: #f7f7f7;
  border: none;
  border-radius: 3px;
  padding: 3px 28px 3px 8px;
  text-align: center;
  margin-top: 12px;
  font-size: 17px;
  color: #929292;
  box-sizing: border-box;
  outline: none;
  &::placeholder {
    color: rgba(146, 146, 146, 0.8);
  }
  ${props => props.isDisabled && 'background-color: #d5e8ef'};
  ${props => props.isDisabled && 'color: #a1bfca'};
  ${props => props.isDisabled && 'cursor: not-allowed'};
`;

const InputContainer = styled.div`
  width: 100%;
  max-width: 340px;
  position: relative;
  margin: 5px 0;
`;

const Label = styled.label`
  font-size: 16px;
  font-family: 'Roboto', sans-serif;
  color: #606060;
  width: 100%;
  display: block;
`;

const IconContainer = styled.button`
  position: absolute;
  top: 43%;
  right: 2%;
  background: none;
  border: none;
  outline: none;
  cursor: pointer;
  background: #f7f7f7;
  svg {
    transition: 0.3s all;
    ${props => props.icon === 'search' && 'transform: scale(1.4)'};
    ${props =>
      props.icon === 'arrow' && `transform: rotate(${props.iconRotate ? '-90deg' : '90deg'})`};
  }
`;

class Input extends React.Component {
  state = {
    input: null,
    value: '',
    showAutoComplete: false,
    dataAutoComplete: null,
    selectValue: null,
    iconRotate: false,
    errorAutoComplete: false,
  };

  componentDidMount() {
    if (!this.props.required) {
      this.isValidated(true);
    } else {
      this.isValidated(false);
    }
    if (this.props.defaultValue) {
      this.myRef.value = this.props.defaultValue;
    }
    if (this.props.select || this.props.autoComplete) {
      document.addEventListener('keyup', this.handleKeyUp);
      document.addEventListener('click', this.handleClick);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.reset !== this.props.reset) {
      this.changeInputValue('', () => this.isValidated(true));
    }
  }

  componentWillUnmount() {
    if (this.props.select || this.props.autoComplete) {
      document.removeEventListener('keyup', this.handleKeyUp);
      document.removeEventListener('click', this.handleClick);
    }
  }

  handleKeyUp = e => {
    if (e.keyCode === 27) {
      this.hiddenList();
    }
  };

  handleClick = e => {
    if (
      e.target !== this.myRef &&
      e.target !== this.iconRef &&
      e.target.parentElement !== this.iconRef
    ) {
      this.hiddenList();
    } else {
      this.showList();
    }
  };

  autoCompleteSelected = ({ value, selectValue }) => {
    this.setState(
      {
        value,
        iconRotate: false,
        selectValue,
        showAutoComplete: false,
        dataAutoComplete: null,
      },
      () => this.isValidated(true)
    );
  };

  typing = e => {
    e.persist();
    const { value } = e.target;

    if (this.props.max && value.length - 1 >= Number(this.props.max)) {
      return;
    }

    if (this.props.data && value.length) {
      const data = this.props.data.filter(item =>
        validations.filterAutoComplete(item, value, this.props.itemFilter)
      );

      this.setState({
        showAutoComplete: true,
        dataAutoComplete: data,
        errorAutoComplete: !data.length,
      });
    } else {
      this.setState({
        showAutoComplete: false,
        dataAutoComplete: null,
        errorAutoComplete: false,
        iconRotate: false,
      });
    }

    if (validations[this.props.type]) {
      if (validations[this.props.type](value)) {
        this.changeInputValue(e, () => this.isValidated(true));
      }
    }

    if (this.props.type === 'plate') {
      this.changeInputValue(e, () => this.isValidated(false));
      if (validations.plate(value)) {
        this.changeInputValue(e, () => this.isValidated(true));
      }
    }

    if (this.props.type === 'text' || this.props.type === 'password') {
      this.changeInputValue(e, () => this.isValidated(true));
    }

    if (this.props.type === 'phone') {
      if (validations.number(value)) {
        this.changeInputValue(e, () => this.isValidated(false));
      }
      if (validations.phone(value)) {
        this.changeInputValue(e, () => this.isValidated(true));
      }
    }

    if (this.props.type === 'email') {
      this.changeInputValue(e, () => this.isValidated(false));
      if (validations.email(value)) {
        this.changeInputValue(e, () => this.isValidated(true));
      }
    }

    if (this.props.required && this.props.min) {
      if (value.length < this.props.min) {
        this.changeInputValue(e, () => this.isValidated(false));
      }
    }

    if (!value && this.props.required) {
      this.changeInputValue('', () => this.isValidated(false));
    }

    if (!value && !this.props.required) {
      this.changeInputValue('', () => this.isValidated(true));
    }
  };

  changeInputValue = (input, callback) => {
    this.setState(
      {
        input,
        value: input.target ? input.target.value : '',
        selectValue: this.props.data && input.target ? input.target.value : '',
      },
      callback
    );
  };

  isValidated = isValid => {
    const input = this.state.input || this.myRef;
    const value = this.state.selectValue || (input.target ? input.target.value : input.value);

    let validated = isValid;

    if (this.props.data && this.props.itemFilter) {
      const data = this.props.data.filter(item => {
        const string = typeof item === 'string' ? item : item[this.props.itemFilter];
        return validations.Normalize(string) === validations.Normalize(value);
      });
      validated = data.length ? isValid : false;
    }
    this.props.onChange({
      input,
      validated,
      value,
    });
  };

  showList = () => {
    this.setState({
      showAutoComplete: true,
      iconRotate: true,
      dataAutoComplete: this.props.data.filter(item =>
        validations.filterAutoComplete(item, '', this.props.itemFilter)
      ),
    });
    this.myRef.focus();
  };

  hiddenList = () => {
    this.setState({
      showAutoComplete: false,
      iconRotate: false,
    });
  };

  render() {
    return (
      <InputContainer>
        <Label htmlFor={this.props.name}>
          {!this.props.select && this.props.label}
          <InputStyled
            innerRef={comp => {
              this.myRef = comp;
            }}
            id={this.props.name}
            data-testid="input"
            name={this.props.name}
            onChange={this.typing}
            value={this.state.value}
            required={this.props.required}
            placeholder={this.props.placeholder}
            type={this.props.type === 'number' ? 'text' : this.props.type}
            onFocus={this.props.select ? this.showList : undefined}
            autoComplete="off"
            readOnly={this.props.select || this.props.disabled}
            isDisabled={this.props.disabled}
            select={this.props.select}
          />
          {(this.props.select || this.props.icon) && (
            <IconContainer
              icon={this.props.icon || 'arrow'}
              iconRotate={this.state.iconRotate}
              innerRef={comp => {
                this.iconRef = comp;
              }}
              onClick={e => {
                e.preventDefault();
                e.stopPropagation();
                if (this.props.select) this.showList();
              }}
            >
              <Icon icon={`${this.props.icon || 'arrow'}`} type="funnel" size="medium" />
            </IconContainer>
          )}
          {this.props.errors.error && (
            <Alert data-testid="inputError" error>
              {this.props.errors.message}
            </Alert>
          )}
          {!this.props.errors.error &&
            this.state.errorAutoComplete && (
              <span style={{ position: 'absolute', top: '100%', left: 0 }}>
                <Alert data-testid="inputError" error>
                  Ninguna encontrada
                </Alert>
              </span>
            )}
        </Label>
        {this.state.showAutoComplete &&
          this.props.autoComplete(this.state.dataAutoComplete, this.autoCompleteSelected)}
      </InputContainer>
    );
  }
}

Input.propTypes = {
  defaultValue: PropTypes.string,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  onChange: PropTypes.func,
  type: PropTypes.string,
  autoComplete: PropTypes.func,
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  itemFilter: PropTypes.string,
  select: PropTypes.bool,
  max: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  min: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  errors: PropTypes.shape({
    error: PropTypes.bool,
    message: PropTypes.string,
  }),
  label: PropTypes.string,
  icon: PropTypes.string,
  reset: PropTypes.bool,
  disabled: PropTypes.bool,
};

Input.defaultProps = {
  placeholder: '',
  required: false,
  onChange: () => {},
  type: 'text',
  autoComplete: null,
  data: null,
  itemFilter: '',
  select: false,
  max: null,
  min: null,
  errors: {},
  label: '',
  icon: null,
  reset: false,
  defaultValue: '',
  disabled: false,
};

export default Input;
