import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';

import Input from './';
import AutoComplete from '../AutoComplete';
import cities from './cities';

storiesOf('Inputs', module)
  .addDecorator(withTests('Input'))
  .add(
    'Type Number',
    withInfo(`
      ~~~js
      // Basic usage
      <Input
        name="cc"
        type="number"
        placeholder="Cedula"
        label="Cedula"
      />

      // Input customization
      <Input
        type="number"
        label="Cedula"
        placheholder="Cedula"
        required={true | false}
        name="cc"
      />
      ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Input type="number" min={3} label="Cedula" placeholder="Cedula" name="cc" required />
      </div>
    ))
  )
  .add(
    'Type Text',
    withInfo(`
      ~~~js
      // Basic usage
      <Input
        name="name"
        type="text"
        placeholder="Primer Nombre"
        label="Primer Nombre"
      />

      // Input customization
      <Input
        type="text"
        label="Primer Nombre"
        placheholder="Primer Nombre"
        required={true | false}
        name="name"
        defaultValue=string
        disabled={true | false}
        onChange={() => {}}
      />
      ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Input
          type="text"
          label="Primer Nombre"
          placeholder="Primer Nombre"
          name="name"
          disabled={false}
          required
        />
      </div>
    ))
  )
  .add(
    'Type Disabled',
    withInfo(`
      ~~~js
      // Basic usage
      <Input
        name="name"
        type="text"
        placeholder="Primer Nombre"
        label="Primer Nombre"
      />

      // Input customization
      <Input
        type="text"
        label="Primer Nombre"
        placheholder="Primer Nombre"
        required={true | false}
        name="name"
        defaultValue=string
        disabled={true | false}
        onChange={() => {}}
      />
      ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Input
          type="text"
          label="Primer Nombre"
          defaultValue="Comparamejor"
          placeholder="Primer Nombre"
          name="name"
          disabled
          required
        />
      </div>
    ))
  )
  .add(
    'Type Email',
    withInfo(`
      ~~~js
      // Basic usage
      <Input
        name="email"
        type="email"
        placeholder="Email"
        label="Email"
      />

      // Input customization
      <Input
        type="text"
        placeholder="Primer Nombre"
        label="Primer Nombre"
        required={true | false}
        name="name"
        onChange={() => {}}
      />
      ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Input type="email" label="Email" placeholder="Email" name="email" required />
      </div>
    ))
  )
  .add(
    'Type Plate',
    withInfo(`
      ~~~js
      // Basic usage
      <Input
        name="email"
        type="plate"
        placeholder="Placa"
        label="Placa"
      />

      // Input customization
      <Input
        type="plate"
        placeholder="Placa"
        label="Placa"
        required={true | false}
        name="plate"
        onChange={() => {}}
      />
      ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Input type="plate" label="Placa" placeholder="Placa" name="plate" required />
      </div>
    ))
  )
  .add(
    'Type Text Autocomplete',
    withInfo(`
      ~~~js
      // Basic usage
      <Input
        name="city"
        type="text"
        placeholder="Ciudad"
        label="Ciudad"
        autoComplete={() => {}}
      />

      // Input customization
      <Input
        type="text"
        placeholder="Ciudad"
        label="Ciudad"
        required={true | false}
        name="city"
        autoComplete={() => {}}
        itemFilter=''
        onChange={() => {}}
      />
      ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Input
          type="text"
          placeholder="Ciudad"
          label="Ciudad"
          name="city"
          required
          data={cities}
          autoComplete={(data, onClick) => (
            <AutoComplete>
              {data.map(city => (
                <AutoComplete.Option changeValue={onClick} value={city} key={city}>
                  {city}
                </AutoComplete.Option>
              ))}
            </AutoComplete>
          )}
        />
      </div>
    ))
  )
  .add(
    'Type Select',
    withInfo(`
      ~~~js
      // Basic usage
      <Input
        name="city"
        type="text"
        select
        placeholder="Ciudad"
        label="Ciudad"
        autoComplete={() => {}}
      />

      // Input customization
      <Input
        type="text"
        placeholder="Ciudad"
        label="Ciudad"
        required={true | false}
        name="city"
        autoComplete={() => {}}
        select
        onChange={() => {}}
      />
      ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Input
          type="text"
          placeholder="Ciudad"
          label="Ciudad"
          name="city"
          required
          select
          data={cities}
          icon="arrow"
          autoComplete={(data, onClick) => (
            <AutoComplete>
              {data.map(city => (
                <AutoComplete.Option changeValue={onClick} value={city} key={city}>
                  {city}
                </AutoComplete.Option>
              ))}
            </AutoComplete>
          )}
        />
      </div>
    ))
  );
