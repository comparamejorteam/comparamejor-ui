export const Normalize = string =>
  String(string)
    .normalize('NFD')
    .replace(/[^\w]/g, '')
    .toLowerCase();

export const number = string => {
  if (string.match(/^[0-9]+$/)) return true;
  return false;
};

export const phone = string => {
  const re = /^3[0-9]{9}$/;
  if (number(string) && re.test(string)) return true;
  return false;
};

export const email = string => {
  /* eslint-disable-next-line */
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (re.test(String(string).toLowerCase())) return true;
  return false;
};

export const plate = string => {
  /* eslint-disable-next-line */
  const re = /^[a-zA-Z]{2,3}[0-9]{2}[a-zA-Z0-9]?$/;

  if (re.test(String(string).toLowerCase())) return true;
  return false;
};

export const filterAutoComplete = (item, value, itemFilter) => {
  const regex = new RegExp(`${value.toLowerCase()}`);

  const string = typeof item === 'string' ? item : item[itemFilter];
  return !!regex.test(Normalize(string));
};
