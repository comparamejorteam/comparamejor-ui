import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';
import Icon from './';

storiesOf('Icons', module)
  .addDecorator(withTests('Icons'))
  .add(
    'Arrow',
    withInfo(`
      ~~~js
      // Basic usage
      <Icon
        icon="arrow"
        size="large"
      />

      // Icon customization
      <Icon
        icon="arrow | download | loading"
        size="large | medium | small"
      />
      ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Icon icon="arrow" size="small" type="funnel" />
        <Icon icon="arrow" size="medium" type="funnel" />
        <Icon icon="arrow" size="large" type="funnel" />
      </div>
    ))
  )
  .add(
    'Loading',
    withInfo(`
      ~~~js
        // Basic usage
        <Icon
          icon="arrow"
          size="large"
        />

        // Icon customization
        <Icon
          icon="arrow | download | loading"
          size="large | medium | small"
        />
        ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Icon icon="loading" size="small" type="funnel" />
        <Icon icon="loading" size="medium" type="funnel" />
        <Icon icon="loading" size="large" type="funnel" />
      </div>
    ))
  )
  .add(
    'Download',
    withInfo(`
      ~~~js
        // Basic usage
        <Icon
          icon="arrow"
          size="large"
          type="funnel"
        />

        // Icon customization
        <Icon
          icon="arrow | download | loading"
          size="large | medium | small"
          type="primary | secondary | funnel | funnel-secondary | special-primary | special-secondary | special-funnel-secondary"
        />
        ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Icon icon="download" size="small" type="funnel" />
        <Icon icon="download" size="medium" type="funnel" />
        <Icon icon="download" size="large" type="funnel" />
      </div>
    ))
  )
  .add(
    'User',
    withInfo(`
      ~~~js
        // Basic usage
        <Icon
          icon="user"
          size="large"
          type="funnel"
        />
        ~~~
    `)(() => (
      <div style={{ padding: '1rem' }}>
        <Icon icon="user" size="small" />
        <Icon icon="user" size="medium" />
        <Icon icon="user" size="large" />
      </div>
    ))
  );
