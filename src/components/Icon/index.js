import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import getStylesByType from '../../utils/getStylesByType';
import * as Icons from './icons';

// styles
import sizes from './sizes.styles';
import types from './types.styles';

const toCamelCase = str => `${str[0].toUpperCase()}${str.slice(1).toLowerCase()}`;

const Icon = ({ icon, className }) => {
  const iconName = toCamelCase(icon);

  if (!Object.getOwnPropertyNames(Icons).includes(iconName)) return null;

  const IconSvg = Icons[iconName];
  return <IconSvg className={className} />;
};

const IconWithStyles = styled(Icon)`
  ${props => getStylesByType(props.size, sizes)};
  ${props => getStylesByType(props.type, types)};
`;

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
};

export default IconWithStyles;
