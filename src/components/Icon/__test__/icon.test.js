import React from 'react';
import styled from 'styled-components';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import Icon from '../';
import sizes from '../sizes.styles';

describe('Icos', () => {
  test('Arrow', () => {
    const tree = renderer.create(<Icon type="funnel" size="large" icon="arrow" />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Loading', () => {
    const tree = renderer.create(<Icon type="funnel" size="large" icon="loading" />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Download', () => {
    const tree = renderer.create(<Icon type="funnel" size="large" icon="download" />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
