import React from 'react';

const CreditCard = () => (
  <svg
    width="40px"
    height="40px"
    viewBox="0 0 40 40"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <defs>
      <path
        d="M2,3.9956504 L2,9.5043496 C2,10.6039327 2.89628405,11.5 3.99922997,11.5 L16.00077,11.5 C17.1041882,11.5 18,10.6052916 18,9.5043496 L18,3.9956504 C18,2.89606731 17.103716,2 16.00077,2 L3.99922997,2 C2.89581178,2 2,2.89470841 2,3.9956504 Z M0,3.9956504 C0,1.78891362 1.79246765,0 3.99922997,0 L16.00077,0 C18.2094838,0 20,1.79269577 20,3.9956504 L20,9.5043496 C20,11.7110864 18.2075324,13.5 16.00077,13.5 L3.99922997,13.5 C1.79051625,13.5 0,11.7073042 0,9.5043496 L0,3.9956504 Z M2,4.5 L2,6.5 L18,6.5 L18,4.5 L2,4.5 Z"
        id="path-vigency"
      />
    </defs>
    <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Icon/Outline/ic_payment">
        <g id="icon">
          <g transform="translate(10.000000, 13.000000)">
            <mask id="mask-vigency" fill="white">
              <use xlinkHref="#path-vigency" />
            </mask>
            <g id="Combined-Shape" fillRule="nonzero" />
            <g id="↪-Color" mask="url(#mask-vigency)">
              <g transform="translate(-14.000000, -17.000000)" id="Color/Blue">
                <g>
                  <rect
                    id="↳-🎨-Color"
                    fill="#00A0E1"
                    x="-1.42e-14"
                    y="3.2e-14"
                    width="48"
                    height="48"
                  />
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);
export default CreditCard;
