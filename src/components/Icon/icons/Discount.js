import React from 'react';

const Discount = () => (
  <svg
    width="22px"
    height="22px"
    viewBox="0 0 22 22"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xlink="http://www.w3.org/1999/xlink"
  >
    <g id="UI" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="1" transform="translate(-31.000000, -765.000000)" fill="#FFFFFF" fillRule="nonzero">
        <g id="Group-10" transform="translate(-10.000000, 574.000000)">
          <g id="Group-9">
            <g id="Group-8" transform="translate(41.000000, 40.000000)">
              <g id="noun_129177_cc" transform="translate(0.000000, 152.000000)">
                <g id="Group">
                  <path
                    d="M9.9825,20.5635 L0.4365,11.0175 L10.4535,1.0005 L15.816,0.2345 L20.7655,5.184 L19.9995,10.5465 L9.9825,20.5635 Z M1.8505,11.0175 L9.9825,19.1495 L19.057,10.075 L19.705,5.538 L15.4625,1.2955 L10.9255,1.9435 L1.8505,11.0175 Z"
                    id="Shape"
                    stroke="#FFFFFF"
                    strokeWidth="0.5"
                  />
                  <path
                    d="M14.225,9.025 C13.624,9.025 13.059,8.791 12.6335,8.3665 C11.7565,7.489 11.7565,6.062 12.6335,5.1845 C13.059,4.7595 13.6235,4.526 14.225,4.526 C14.8255,4.526 15.3905,4.76 15.816,5.1845 C16.693,6.062 16.693,7.489 15.816,8.3665 C15.3905,8.791 14.8255,9.025 14.225,9.025 Z M14.225,5.5255 C13.891,5.5255 13.577,5.6555 13.3405,5.891 C12.8535,6.3785 12.8535,7.172 13.3405,7.659 C13.8125,8.13 14.6355,8.131 15.1085,7.659 C15.5955,7.1715 15.5955,6.378 15.1085,5.891 C14.8725,5.6555 14.5585,5.5255 14.225,5.5255 Z"
                    id="Shape"
                  />
                  <rect
                    id="Rectangle-path"
                    transform="translate(9.805585, 11.194303) rotate(45.000000) translate(-9.805585, -11.194303) "
                    x="6.55561588"
                    y="10.6943034"
                    width="6.49993766"
                    height="1"
                  />
                  <rect
                    id="Rectangle-path"
                    transform="translate(8.391606, 12.608353) rotate(45.000000) translate(-8.391606, -12.608353) "
                    x="5.14163708"
                    y="12.1083534"
                    width="6.49993766"
                    height="1"
                  />
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default Discount;
