import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Search = ({ className }) => (
  <svg viewBox="0 0 20 20" className={`search ${className}`}>
    <path d="M19,16.33l-4-4-.07-.06a7.62,7.62,0,1,0-2.19,2.19l.06.07,4,4A1.55,1.55,0,0,0,19,16.33ZM8.54,13a5,5,0,1,1,5-5,5,5,0,0,1-5,5Zm0,0" />
  </svg>
);

Search.propTypes = {
  className: PropTypes.string.isRequired,
};

export default Search;
