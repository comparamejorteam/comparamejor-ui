import React, { Component } from 'react';
import moment from 'moment';
import ReactDatePicker from 'react-datepicker';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Input from './Input';
import './styles.css';

const Container = styled.div`
  width: 100%;
  position: relative;
`;

moment.locale('es');

class DatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment(),
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date) {
    this.setState({
      startDate: date,
    });

    this.props.onChange(this.props.name, date);
  }

  render() {
    return (
      <Container>
        <ReactDatePicker
          customInput={<Input name={this.props.name} label={this.props.label} />}
          calendarClassName="date-picker-custom"
          selected={this.state.startDate}
          onChange={this.handleChange}
          minDate={this.props.minDate && moment()}
          maxDate={this.props.maxDate && moment().add(this.props.maxDate, 'days')}
        />
      </Container>
    );
  }
}

DatePicker.propTypes = {
  minDate: PropTypes.bool,
  maxDate: PropTypes.number,
  name: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
};

DatePicker.defaultProps = {
  minDate: 0,
  maxDate: 0,
  name: '',
  label: '',
  onChange: () => {},
};

export default DatePicker;
