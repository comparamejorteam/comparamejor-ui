import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Icon from '../Icon';

const InputContainer = styled.label`
  display: flex;
  width: 100%;
  max-width: 380px;
  height: 60px;
  border-radius: 6px;
  overflow: hidden;
`;

const Span = styled.span`
  display: block;
  color: #9b9b9b;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  margin-bottom: 7px;
`;

const Input = styled.input`
  border: none;
  width: 100%;
  padding: 1rem;
  background-color: #f7f7f7;
  color: #00376b;
  font-size: 16px;
`;

const IconContainer = styled.div`
  padding: 1rem;
  background-color: #009ad3;
  color: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

const Container = props => (
  <div style={{ position: 'relative' }}>
    <Span>{props.label}</Span>
    <InputContainer>
      <Input onClick={props.onClick} value={props.value} name={props.name} readOnly />
      <IconContainer>
        <Icon icon="calendar" />
      </IconContainer>
    </InputContainer>
  </div>
);

Container.propTypes = {
  onClick: PropTypes.func,
  value: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
};

Container.defaultProps = {
  onClick: () => {},
  label: '',
  name: '',
  value: '',
};

export default Container;
