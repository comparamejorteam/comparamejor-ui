import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';

import DatePicker from './';

storiesOf('DatePicker', module).add(
  'DatePicker',
  withInfo(`
    ~~~js
    // Basic usage
    <DatePicker
      name={string}
      minDate={bool}
      maxDate={number}
      label={string}
      onChange={function}
    />
    ~~~
  `)(() => (
    <div style={{ padding: '1rem' }}>
      <DatePicker name="date" minDate maxDate={90} label="Ingrese fecha" onChange={console.log} />
    </div>
  ))
);
