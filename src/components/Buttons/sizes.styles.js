import { css } from 'styled-components';
import Icon from '../Icon';

const addIconPosition = (props, size) => {
  if (
    props.type === 'primary' ||
    props.type === 'secondary' ||
    props.type === 'funnel' ||
    props.type === 'funnel-secondary'
  ) {
    return null;
  }
  return size;
};

const small = css`
  border-radius: 4px;
  font-size: 14px;
  height: 40px;
  width: 131px;
  max-width: 180px;

  & ${Icon} {
    left: ${props => addIconPosition(props, '50px;')};
  }

  &:hover ${Icon} {
    right: 5px;
  }

  @media screen and (max-width: 576px) {
    height: 42px;
    max-width: 100%;
  }

  ${props => props.icon !== 'null' && 'padding-right: 17px;'};
`;

const medium = css`
  border-radius: 5px;
  font-size: 17px;
  height: 59px;
  width: 180px;
  max-width: 200px;

  & ${Icon} {
    left: ${props => addIconPosition(props, '50px;')};
  }

  &:hover ${Icon} {
    right: 12px;
  }

  @media screen and (max-width: 576px) {
    height: 50px;
    max-width: 100%;
  }

  ${props => props.icon !== 'null' && 'padding-right: 24px;'};
`;

const large = css`
  border-radius: 6px;
  font-size: 20px;
  height: 77px;
  width: 234px;
  max-width: 265px;

  & ${Icon} {
    left: ${props => addIconPosition(props, '95px;')};
  }

  &:hover ${Icon} {
    right: 10px;
  }

  @media screen and (max-width: 576px) {
    height: 58px;
    max-width: 100%;
  }

  ${props => props.icon !== 'null' && 'padding-right: 32px;'};
`;

const largexl = css`
  border-radius: 6px;
  font-size: 20px;
  height: 77px;
  width: 265px;
  max-width: 265px;

  & ${Icon} {
    right: 25px;
    top: 38%;
  }

  &:hover ${Icon} {
    right: 15px;
  }

  @media screen and (max-width: 576px) {
    height: 58px;
    max-width: 100%;
  }

  ${props => props.icon !== 'null' && 'padding-right: 32px;'};
`;

export default {
  small,
  medium,
  large,
  largexl,
};
