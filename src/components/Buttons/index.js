import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { colors } from '../../shared/variables';
import getStylesByType from '../../utils/getStylesByType';

import Icon from '../Icon';
import sizes from './sizes.styles';
import types from './types.styled';

const Children = props => (
  <span>
    {props.icon && <Icon {...props} />}
    <span className="btn-text">{props.text}</span>
  </span>
);

Children.propTypes = {
  icon: PropTypes.string,
  text: PropTypes.string,
};

Children.defaultProps = {
  icon: '',
  text: '',
};

const Button = styled.button.attrs({
  children: props => <Children {...props} />,
})`
  border: none;
  box-sizing: border-box;
  font-family: 'Roboto', sans-serif;
  padding: 0;
  position: relative;
  transition: 0.05s;
  cursor: pointer;

  & ${Icon} {
    align-items: center;
    display: flex;
    justify-content: center;
    position: absolute;
    bottom: 0;
    transition: 0.3s;
  }

  & svg.download,
  & svg.phone {
    height: calc(1em*1.875);
    top: calc(50% - (1em*0.875));
    width: calc(1em*1.875);
  }

  ${props =>
    props.demostration &&
    css`
      margin: 10px;
    `};

  @media screen and (max-width: 576px) {
    width: 100%;
    padding-right: 0;

    ${props =>
      props.demostration &&
      css`
        margin: 10px auto;
      `};

    & ${Icon}:not(.phone):not(.download) {
      left: calc(100% - (1em * 1.875));
    }

    &:hover ${Icon}:not(.phone):not(.download) {
      left: calc(100% - 1.4em);
    }

    & ${Icon}.phone,
    & ${Icon}.download,
    &:hover ${Icon}.phone,
    &:hover ${Icon}.download {
      left: calc(50% - (1em * 0.875));
    }
  }
`.extend`${props => getStylesByType(props.size, sizes)}`.extend`${props =>
  getStylesByType(props.type, types)}`.extend`${props => props.disabled && types.disabled}`;

Button.propTypes = {
  type: PropTypes.oneOf([
    'primary',
    'secondary',
    'funnel',
    'funnel-secondary',
    'special-primary',
    'special-secondary',
    'special-funnel-secondary',
  ]),
  size: PropTypes.oneOf(['largexl', 'large', 'medium', 'small']),
  icon: PropTypes.oneOf(['arrow', 'loading', 'download', 'phone', 'null']),
  text: PropTypes.string.isRequired,
  demostration: PropTypes.bool,
};

Button.defaultProps = {
  type: 'primary',
  size: 'large',
  icon: 'arrow',
  demostration: false,
};

export default Button;
