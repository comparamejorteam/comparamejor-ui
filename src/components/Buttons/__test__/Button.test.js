import React from 'react';
import styled from 'styled-components';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import Button from '../';

test('Large size', () => {
  const tree = renderer
    .create(<Button text="Entendido" size="large" type="primary" icon="null" />)
    .toJSON();
  expect(tree).toMatchSnapshot();

  expect(tree).toHaveStyleRule('height', '77px');
});

test('Medium size', () => {
  const tree = renderer
    .create(<Button type="primary" text="Entendido" size="medium" icon="null" />)
    .toJSON();
  expect(tree).toMatchSnapshot();

  expect(tree).toHaveStyleRule('height', '59px');
});

test('Small size', () => {
  const tree = renderer
    .create(<Button type="primary" text="Entendido" size="small" icon="null" />)
    .toJSON();
  expect(tree).toMatchSnapshot();

  expect(tree).toHaveStyleRule('height', '40px');
});

test('Primary type', () => {
  const tree = renderer.create(<Button type="primary" text="Entendido" icon="null" />).toJSON();
  expect(tree).toMatchSnapshot();

  // Without effects
  expect(tree).toHaveStyleRule('background-color', '#42B715');
  expect(tree).toHaveStyleRule('color', '#FFFFFF');

  // Hover
  expect(tree).toHaveStyleRule('background-color', '#3c9916', {
    modifier: ':hover',
  });

  // Active
  expect(tree).toHaveStyleRule('background-color', '#3c9916', {
    modifier: ':active',
  });
  expect(tree).toHaveStyleRule('color', 'rgba(12,12,12,.5)', {
    modifier: ':active',
  });
});

test('Secondary type', () => {
  const tree = renderer.create(<Button type="secondary" text="Entendido" icon="null" />).toJSON();
  expect(tree).toMatchSnapshot();

  // Without effects
  expect(tree).toHaveStyleRule('background-color', '#009BD2');
  expect(tree).toHaveStyleRule('color', '#FFFFFF');

  // Hover
  expect(tree).toHaveStyleRule('background-color', '#4f8db7', {
    modifier: ':hover',
  });

  // Active
  expect(tree).toHaveStyleRule('background-color', '#4f8db7', {
    modifier: ':active',
  });
  expect(tree).toHaveStyleRule('color', 'rgba(12,12,12,.5)', {
    modifier: ':active',
  });
});

test('Funnel type', () => {
  const tree = renderer.create(<Button type="funnel" text="Entendido" icon="null" />).toJSON();
  expect(tree).toMatchSnapshot();

  // Without effects
  expect(tree).toHaveStyleRule('background-color', '#FFFFFF');
  expect(tree).toHaveStyleRule('border', '3px solid #004B8C');
  expect(tree).toHaveStyleRule('color', '#004B8C');

  // Hover
  expect(tree).toHaveStyleRule('background-color', '#004B8C', {
    modifier: ':hover',
  });
  expect(tree).toHaveStyleRule('color', '#FFFFFF', {
    modifier: ':hover',
  });

  // Active
  expect(tree).toHaveStyleRule('background-color', '#004B8C', {
    modifier: ':active',
  });
  expect(tree).toHaveStyleRule('color', 'rgba(12,12,12,.5)', {
    modifier: ':active',
  });
});

test('Funnel secondary type', () => {
  const tree = renderer
    .create(<Button type="funnel-secondary" text="Entendido" icon="null" />)
    .toJSON();
  expect(tree).toMatchSnapshot();

  // Without effects
  expect(tree).toHaveStyleRule('background-color', 'transparent');
  expect(tree).toHaveStyleRule('border', '3px solid #FFFFFF');
  expect(tree).toHaveStyleRule('color', '#FFFFFF');

  // Hover
  expect(tree).toHaveStyleRule('background-color', '#FFFFFF', {
    modifier: ':hover',
  });
  expect(tree).toHaveStyleRule('color', '#004B8C', {
    modifier: ':hover',
  });

  // Active
  expect(tree).toHaveStyleRule('background-color', '#FFFFFF', {
    modifier: ':active',
  });
  expect(tree).toHaveStyleRule('color', '#929292', {
    modifier: ':active',
  });
});

test('Disabled', () => {
  const tree = renderer.create(<Button disabled text="Entendido" icon="null" />).toJSON();
  expect(tree).toMatchSnapshot();

  // Without effects
  expect(tree).toHaveStyleRule('background-color', '#929292');
  expect(tree).toHaveStyleRule('color', '#FFFFFF');

  // Hover
  expect(tree).toHaveStyleRule('background-color', '#929292', {
    modifier: ':hover',
  });
  expect(tree).toHaveStyleRule('color', '#FFFFFF', {
    modifier: ':hover',
  });
});
