import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';
import Button from './';

storiesOf('Buttons', module)
  .addDecorator(withTests('Button'))
  .add(
    'Button principal',
    withInfo(`
      ~~~js
      // Basic usage
      <Button
        type="primary"
        size="large"
        text="Cotiza Gratis"
      />
      
      // Button customization
      <Button
        type="
        primary | secondary | funnel | funnel-secondary | special-primary | special-secondary | special-funnel-secondary"
        size="large | medium | small"
        text="Cotiza Gratis"
        withIcon="arrow | loading"
      />
      ~~~
    `)(() => (
      <div>
        <Button type="primary" size="small" icon="arrow" text="Cotiza Gratis" demostration />
        <Button type="primary" size="medium" icon="arrow" text="Cotiza Gratis" demostration />
        <Button type="primary" size="large" icon="arrow" text="Cotiza Gratis" demostration />
        <Button
          type="primary"
          size="largexl"
          icon="arrow"
          text="Cotiza tu seguro sin placa"
          demostration
        />
      </div>
    ))
  )
  .add(
    'Button secundario',
    withInfo(`
      ~~~js
      // Basic usage
      <Button
        type="secondary"
        size="large"
        text="Cotiza Gratis"
      />
      
      // Loading Button
      <Button
        type="secondary"
        size="large"
        text="Cotiza Gratis"
        icon="loading" // this line is important
      />
      
      // Button customization
      <Button
        type="
        primary | secondary | funnel | funnel-secondary | special-primary | special-secondary | special-funnel-secondary"
        size="large | medium | small"
        text="Cotiza Gratis"
        withIcon="arrow | loading"
      />
      ~~~
    `)(() => (
      <div>
        <Button type="secondary" size="small" icon="arrow" text="Cotiza Gratis" demostration />
        <Button type="secondary" size="medium" icon="arrow" text="Cotiza Gratis" demostration />
        <Button type="secondary" size="large" icon="arrow" text="Cotiza Gratis" demostration />
        <Button
          type="secondary"
          size="largexl"
          icon="arrow"
          text="Cotiza tu seguro sin placa"
          demostration
        />
      </div>
    ))
  )
  .add(
    'Button funnel',
    withInfo(`
      ~~~js
        // Basic usage
        <Button
          type="funnel"
          size="large"
          text="Enviar"
        />
        
        // Button customization
        <Button
          type="primary | secondary | funnel | funnelSecondary"
          size="large | medium | small"
          text="Cotiza Gratis"
          withIcon="arrow | loading"
        />
      ~~~
    `)(() => (
      <div>
        <Button type="funnel" size="small" icon="arrow" text="Enviar" demostration />
        <Button type="funnel" size="medium" icon="arrow" text="Enviar" demostration />
        <Button type="funnel" size="large" icon="arrow" text="Enviar" demostration />
        <Button type="funnel" size="largexl" icon="arrow" text="Enviar" demostration />
      </div>
    ))
  )
  .add(
    'Button funnel secundario',
    withInfo(`
      ~~~js
      // Basic usage
      <Button
        type="funnel-secondary"
        size="large"
        text="Cotiza Gratis"
      />
      
      // Button customization
      <Button
        type="
        primary | secondary | funnel | funnel-secondary | special-primary | special-secondary | special-funnel-secondary"
        size="large | medium | small"
        text="Cotiza Gratis"
        withIcon="arrow | loading"
      />
      ~~~
    `)(() => (
      <div style={{ backgroundColor: '#004B8C', padding: '20px 0', width: '100%' }}>
        <Button type="funnel-secondary" size="small" icon="arrow" text="Enviar" demostration />
        <Button type="funnel-secondary" size="medium" icon="arrow" text="Enviar" demostration />
        <Button type="funnel-secondary" size="large" icon="arrow" text="Enviar" demostration />
        <Button
          type="funnel-secondary"
          size="largexl"
          icon="arrow"
          text="Cotiza tu seguro sin placa"
          demostration
        />
      </div>
    ))
  )
  .add(
    'Button special principal',
    withInfo(`
      ~~~js
      // Basic usage
      <Button
        type="special-primary"
        size="small"
        text="Boton desabilitado"
      />
      
      // Button customization
      <Button
        type="
        primary | secondary | funnel | funnel-secondary | special-primary | special-secondary | special-funnel-secondary"
        size="large | medium | small"
        text="Cotiza Gratis"
        withIcon="arrow | loading"
      />
      ~~~
    `)(() => (
      <div>
        <Button type="special-primary" size="small" icon="phone" text="Phone" phone demostration />
        <Button type="special-primary" size="medium" icon="phone" text="Enviar" demostration />
        <Button type="special-primary" size="large" icon="phone" text="Enviar" demostration />
        <Button type="special-primary" size="largexl" icon="phone" text="Enviar" demostration />
      </div>
    ))
  )
  .add(
    'Button special secundario',
    withInfo(`
      ~~~js
      // Basic usage
      <Button
        type="special-primary"
        size="small"
        text="Boton desabilitado"
      />
      
      // Button customization
      <Button
        type="
        primary | secondary | funnel | funnel-secondary | special-primary | special-secondary | special-funnel-secondary"
        size="large | medium | small"
        text="Cotiza Gratis"
        withIcon="arrow | loading"
      />
      ~~~
    `)(() => (
      <div>
        <Button type="special-secondary" size="small" icon="phone" text="Enviar" demostration />
        <Button
          type="special-secondary"
          size="small"
          icon="download"
          text="Descargar"
          demostration
        />
        <Button type="special-secondary" size="medium" icon="phone" text="Enviar" demostration />
        <Button
          type="special-secondary"
          size="medium"
          icon="download"
          text="Descargar"
          demostration
        />
        <Button type="special-secondary" size="large" icon="phone" text="Enviar" demostration />
        <Button
          type="special-secondary"
          size="large"
          icon="download"
          text="Descargar"
          demostration
        />
      </div>
    ))
  )
  .add(
    'Button special funnel secundario',
    withInfo(`
      ~~~js
      // Basic usage
      <Button
        type="special-primary"
        size="small"
        text="Boton desabilitado"
      />
      
      // Button customization
      <Button
        type="
        primary | secondary | funnel | funnel-secondary | special-primary | special-secondary | special-funnel-secondary"
        size="large | medium | small"
        text="Cotiza Gratis"
        withIcon="arrow | loading"
      />
      ~~~
    `)(() => (
      <div style={{ backgroundColor: '#009BD2', padding: '20px 0', width: '100%' }}>
        <Button
          type="special-funnel-secondary"
          size="small"
          icon="phone"
          text="Enviar"
          demostration
        />
        <Button
          type="special-funnel-secondary"
          size="medium"
          icon="phone"
          text="Enviar"
          demostration
        />
        <Button
          type="special-funnel-secondary"
          size="large"
          icon="phone"
          text="Enviar"
          demostration
        />
      </div>
    ))
  )
  .add(
    'Button disabled',
    withInfo(`
      ~~~js
      // Basic usage
      <Button
        size="small"
        text="Boton desabilitado"
        disabled
      />
      
      // Button customization
      <Button
        type="
        primary | secondary | funnel | funnel-secondary | special-primary | special-secondary | special-funnel-secondary"
        size="large | medium | small"
        text="Cotiza Gratis"
        withIcon="arrow | loading"
      />
      ~~~
    `)(() => (
      <div>
        <Button size="small" icon="arrow" text="Enviar" disabled demostration />
        <Button size="medium" icon="arrow" text="Enviar" disabled demostration />
        <Button size="large" icon="arrow" text="Enviar" disabled demostration />
      </div>
    ))
  );
