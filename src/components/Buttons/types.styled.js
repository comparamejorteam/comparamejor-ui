import { css } from 'styled-components';
import { colors } from '../../shared/variables';
import Icon from '../Icon';

function IconStyles({ hoverColor, activeColor }) {
  return css`
    &:hover ${Icon} path {
      fill: ${hoverColor};
    }
    &:active ${Icon} path {
      fill: ${activeColor};
    }
  `;
}

const primary = css`
  background-color: ${colors.green};
  color: ${colors.white};

  &:hover,
  &:active {
    background-color: ${colors.darkGreen};
  }
  &:active {
    color: ${colors.pushColor};
  }

  ${IconStyles({
    hoverColor: colors.white,
    activeColor: colors.pushColor,
  })};
`;

const secondary = css`
  background-color: ${colors.blue};
  color: ${colors.white};

  &:hover,
  &:active {
    background-color: ${colors.hoverBlue};
  }
  &:active {
    color: ${colors.pushColor};
  }

  ${IconStyles({
    hoverColor: colors.white,
    activeColor: colors.pushColor,
  })};
`;

const funnel = css`
  background-color: ${colors.white};
  border: 3px solid ${colors.darkBlue};
  color: ${colors.darkBlue};

  &:hover,
  &:active {
    background-color: ${colors.darkBlue};
    color: ${colors.white};
  }
  &:active {
    color: ${colors.pushColor};
  }
  ${IconStyles({
    hoverColor: colors.white,
    activeColor: colors.pushColor,
  })};
`;

const funnelSecondary = css`
  background-color: transparent;
  border: 3px solid ${colors.white};
  color: ${colors.white};

  &:hover,
  &:active {
    background-color: ${colors.white};
    color: ${colors.darkBlue};
  }
  &:active {
    color: ${colors.gray};
  }

  ${IconStyles({
    hoverColor: colors.darkBlue,
    activeColor: colors.gray,
  })};
`;

const specialPrimary = css`
  background-color: ${colors.white};
  border: 3px solid ${colors.green};
  color: ${colors.green};
  padding-right: 0;

  &:hover,
  &:active {
    color: ${colors.white};
  }

  &:active {
    background-color: ${colors.green};
    color: ${colors.green};
  }

  ${IconStyles({
    hoverColor: colors.green,
    activeColor: colors.white,
  })};

  & ${Icon} {
    display: none;
    right: 0;
  }

  &:hover ${Icon} {
    display: block;
  }

  ${props =>
    props.phone &&
    css`
      @media screen and (max-width: 576px) {
        width: 42px !important;
        height: 42px !important;
        & {
          color: ${colors.white};
          path {
            fill: ${colors.green};
          }
          &:hover {
            background: ${colors.green};
            path {
              fill: ${colors.white} !important;
            }
          }
        }
        & ${Icon} {
          display: block !important;
          &:hover {
            fill: ${colors.white};
          }
        }
        .btn-text {
          display: none;
        }
      }
    `};
`;

const specialSecondary = css`
  background-color: ${colors.white};
  border: 3px solid ${colors.hoverBlue};
  color: ${colors.hoverBlue};
  padding-right: 0;

  &:hover,
  &:active {
    color: ${colors.white};
  }

  &:active {
    background-color: ${colors.hoverBlue};
    color: ${colors.hoverBlue};
  }

  ${IconStyles({
    hoverColor: colors.hoverBlue,
    activeColor: colors.white,
  })};

  & ${Icon} {
    display: none;
    right: 0;
  }

  &:hover ${Icon} {
    display: block;
  }
`;

const specialFunnelSecondary = css`
  background-color: transparent;
  border: 3px solid ${colors.white};
  color: ${colors.white};
  padding-right: 0;

  &:hover,
  &:active {
    color: ${colors.blue};
  }
  &:active {
    background-color: ${colors.white};
    color: ${colors.white};
  }

  ${IconStyles({
    hoverColor: colors.white,
    activeColor: colors.blue,
  })};

  & ${Icon} {
    display: none;
    right: 0;
  }

  &:hover ${Icon} {
    display: block;
  }
`;

const disabled = css`
  background-color: ${colors.gray};
  color: ${colors.white};
  padding-right: 0;
  cursor: not-allowed;

  &:hover {
    background-color: ${colors.gray};
    color: ${colors.white};
  }

  & ${Icon} {
    display: none;
  }
`;

export default {
  primary,
  secondary,
  funnel,
  disabled,
  'funnel-secondary': funnelSecondary,
  'special-primary': specialPrimary,
  'special-secondary': specialSecondary,
  'special-funnel-secondary': specialFunnelSecondary,
};
