import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import withTests from '../../../stories/withTests';
import Alert from './';

storiesOf('Alerts', module)
  .addDecorator(withTests('Alert'))
  .add(
    'Alert Success',
    withInfo(`
      ~~~js
      // Basic usage
      <Alert success>Alert text</Alert>
      ~~~
    `)(() => (
      <div>
        <Alert success>*No es necesario que contestes esta pregunta.</Alert>
      </div>
    ))
  )
  .add(
    'Alert Error',
    withInfo(`
      ~~~js
      // Basic usage
      <Alert error>Alert text</Alert>
      ~~~
    `)(() => (
      <div>
        <Alert error>*Es necesario que contestes esta pregunta.</Alert>
      </div>
    ))
  );
