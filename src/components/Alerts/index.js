import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { colors } from '../../shared/variables';

const Alert = styled.span`
  color: ${colors.black};
  font-size: 16px;
  font-family: 'Roboto', sans-serif;
  @media screen and (max-width: 767px) {
    font-size: 14px;
  }
  ${props =>
    props.success &&
    css`
      font-size: 16px;
      color: ${colors.green};
    `};
  ${props =>
    props.error &&
    css`
      font-size: 16px;
      color: ${colors.red};
    `};
`;

Alert.propTypes = {
  success: PropTypes.bool,
  error: PropTypes.bool,
};

Alert.defaultProps = {
  success: false,
  error: false,
};

export default Alert;
