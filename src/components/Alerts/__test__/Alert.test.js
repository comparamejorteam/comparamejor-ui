import React from 'react';
import styled from 'styled-components';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import Alert from '../';

test('Alert success', () => {
  const tree = renderer.create(<Alert success>Text</Alert>).toJSON();
  expect(tree).toMatchSnapshot();
  // expect(tree).toHaveStyleRule('background-color', '#42B715');
});

test('Alert error', () => {
  const tree = renderer.create(<Alert error>Text</Alert>).toJSON();
  expect(tree).toMatchSnapshot();
  // expect(tree).toHaveStyleRule('background-color', '#F55');
});
