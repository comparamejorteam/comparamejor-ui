export default (type, styles) => Object.getOwnPropertyNames(styles).includes(type) && styles[type];
